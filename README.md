# Django

Fullstack application for **Django**

## <u>How to test the app</u>

- Se login avec le compte `Linus`
- Créer un compte dans un navigateur privé
- Créer un password_tag via Linus
- Partager un password_tag avec le 2ème compte
- Mettre à jour le password_tag (url par exemple)
- Regarder l'history
- Supprimer le password_tag

---

## <u>▶ Versions</u>

- Python 3.11.2 https://www.python.org/downloads/

---

## <u>🚀 Getting started</u>

- Clone this project with access token or sshkey even if it's public
    - `git clone https://gitlab.com/mickael.lebas/password_manager.git`
- Install dependencies
    - `pip install -r requirements.txt`

---

## <u>🔨 dev</u>

- Install pre-commit (black and flake8)
    - `pre-commit install`

---

## <u>💻 Commands</u>

### <u>Python</u>

- Run server (can change 8000 by watever you want for port or remove it)
    - `python ./src/manage.py runserver 8000`

---

## <u>🧠 Extra implementations</u>

Le métier de développeur à notre sense se base sur la curiosité et l'intérêt qu'on y porte : la passion.
Afin d'affiner nos compétences et notre expérience sur Django, nous avons décidé de pousser le framework au maximum que
l'on pouvait avec les délais imparti.

### <u>Register & Login</u>

Pour réaliser cette feature, nous avons utilisés les objets de django admin dans son intégralité, model compris sans
avoir à tout réécrire. De plus, nous avons créé un middleware "guard" pour vérifier si l'utilisateur est bien connecté.
Les routes sont configurables dans le fichier settings.py.

```
# Exempt urls of a no authenticated
LOGIN_EXEMPT_URLS = [reverse_lazy("register")]
```

### <u>Templating</u>

Pour réaliser toute la partie templating, nous avons utilisés un autre framework / template engine : Jinja2.
Jinja2 est un moteur de template extrêmement puissant capable d'écrire du code plus simplement et d'avoir de meilleurs
performances.
Il interprête encore mieux le langage python.

Plus sur le sujet a cette
adresse : [Benchmark](https://www.dyspatch.io/blog/python-templating-performance-showdown-django-vs-jinja/)

Nous avons simplifier toutes nos vues pour qu'elles utilisent pleinement toute la puissance de jinja avec une multitude
de block réutilisable ou réécrivable.
Objective less code.
Pour écrire des formulaires magnifique, sans que les class soient écrite au pire endroit qu'il soit (dans les Form par
exemple), nous avons décidé de réécrire toute l'architecture des champs à la main en utilisant l'objet form de django.

Toujours dans la démarche de perfectionnement, nous avons implémenté quelques filtres custom avec jinja pour les
réutiliser dans le template.

### <u>Logger</u>

Rien de mieux qu'un logger custom via un Middleware avec une coloration syntaxique splandide ?

![img.png](img/logger.png)

### <u>Views</u>

Sauf exception, les views sont rendu grâce au package views de django via l'import
generic : `from django.views import generic`
Il existe une multitude de GenericView telles que ListView, CreateView, UpdateView, DeleteView.
Pour les utiliser pleinement il suffit d'override les variables de base et ou les fonctions si on veut être encore plus
précis.

### <u>History</u>

L'historique comme a été demandé, a été fait intégrâlement à la main et qui plus est dynamique.
Nous avons utilisé plusieurs principe de base de django.

#### <u>📡 Signals</u>

A commencer par les Signals inclu dans le package db de
django : `from django.db.models.signals import post_save, pre_save`.
L'objet Signal est un objet appelé à un certain moment dans l'application sur le cycle de vie d'un model.
Le Signal `pre_save` a été implémenté pour récupérer l'ancienne instance du model.
Le Signal `post_save` a été implémenté pour récupérer la nouvelle instance du model.

#### <u>Dynamic</u>

Après avoir implémenter les signals, avoir récupérer l'ancienne et la nouvelle instance, il fallait maintenant comparer
les 2 et en faire un historique.

On pourrait très bien améliorer nos models pour persister l'ancienne et la nouvelle valeur, mais nous avons décidé dans
une première version de ne mettre que l'ancienne valeur si elle a été modifiée.

La première étape consiste à vérifier que les 2 instances proviennent du même model.

La deuxième étape consiste à récupérer le nom de class d'une des 2 instances, de la concatener avec `History` (si le
code est bien fait, et c'est le cas, la table d'historique a le même nom que le model suivi du suffixe `History`) puis
de créer une instance de ce model via sa définition.

On transforme les 2 instances en dictionnaire de données sans `_state`, on boucle sur les propriétés, on regarde si a et
b ont la même valeur, si ce n'est pas le cas, on set l'ancienne valeur dans l'historique.

La dernière étape consiste à vérifier qu'il y est eu au moins un changement, si c'est le cas, on save les données.

Dans une perspective d'amélioration, on pourrait décrypter des champs et/ou permettre de les décrypter via un bouton. On
aurait pu les décrypter, mais puisqu'on a déjà implémenté cette fonctionnalité dans d'autres vues, nous avons
prévilégier notre temps à la recherche poussé du framework.

### <u>Testing</u>

Un petit mot sur les tests unitaires avant de conclure.
La partie testing n'est pas toujours simple à faire ou à mettre en place mais cela permet toujours de corriger les
erreurs bêtes, surtout dans les tests unitaires.
Le test ultime a été fait avec selenium, création d'un compte, authentification, création d'un PasswordTag.
Selenium seul ne permet pas de récupérer la request par exemple, nous avons donc utilisé un wrapper qui s'appelle
seleniumwire.
Pour lancer les tests: `python manage.py test`

### <u>Cryptage</u>

Nous avons créé une commande pour générer une clé de cryptage qui doit être ajoutée au fichier settings.py. Par exemple,
vous pouvez l'exécuter en utilisant la commande suivante `python manage.py gen_salf_key`
exemple :

```
$ python manage.py gen_salt_key
$ Do you want to generate a salf key ? (y = yes, n = no) y
$ YOUR KEY: 
$ b'_fb62GhF9O96Ng0CQOBlBO5-IQkhc6zuGNGTFwlbRBo='
```

⚠️⚠️⚠️ Nous avons déjà une clé dans le fichier settings.py afin de limiter la nécessité de trop de configurations lors
de la correction. Cependant, il est évident que cette approche n'est pas du tout recommandée. ⚠️⚠️⚠️

### <u>EndGame</u>

- Un petit search rapide en js.
- Permissions dans des mixins.
- Pre commit avec Flake8 / black

## <u>🔥 End</u>

Que dire de plus si ce n'est que le projet était très fun à réaliser, qu'on a réussi à aller au bout des concepts et que
nous avons essayé de tout optimisé au maximum. Il y a forcément des pistes d'améliorations car rien n'est parfait mais
cela va s'en dire, mais énormément de choses et de principes ont été impléménté sur ce projet.

Le debugging avec django c'est vraiment pas très pratique, il faut utiliser `pprint` toutes les 3 secondes et on peut
pas descendre dans l'arborescence de l'objet sans refaire un `pprint`, mais on y arrive quand même.

# Happy coding 🤗