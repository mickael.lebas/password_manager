from django.contrib import admin
from .models import User


class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "username",
        "is_active",
    )


admin.site.register(User, AccountAdmin)
