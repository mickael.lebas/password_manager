from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import (
    AuthenticationForm,
    UserCreationForm,
    UsernameField,
)
from django.utils.translation import gettext_lazy as _

from .models import User


class SuperAuthenticationForm(AuthenticationForm):
    """
    Override the default class to add css class on fields
    """

    username = UsernameField(
        widget=forms.TextInput(attrs={"autofocus": True})
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(
            attrs={"autocomplete": "current-password"}
        ),
    )


class SignUpForm(UserCreationForm):
    """
    Override the default class to add css class on fields
    """

    username = UsernameField(
        widget=forms.TextInput(attrs={"autofocus": True})
    )
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(
            attrs={"autocomplete": "new-password"}
        ),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput(
            attrs={"autocomplete": "new-password"}
        ),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    class Meta:
        model = User
        fields = ("username", "password1", "password2")


class ChangeMyMindForm(forms.ModelForm):
    username = UsernameField(widget=forms.TextInput(attrs={"autofocus": True}))

    class Meta:
        model = User
        fields = ("username",)
