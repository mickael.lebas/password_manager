from django.urls import include, path

from .views import ChangeMyMind, SignUpView, SuperLoginView

urlpatterns = [
    path("login/", SuperLoginView.as_view(), name="login"),
    path("register/", SignUpView.as_view(), name="register"),
    path("changedmymind/", ChangeMyMind.as_view(), name="changedmymind"),
    path("", include("django.contrib.auth.urls")),
]
