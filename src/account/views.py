from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from django.views import generic

from .form import ChangeMyMindForm, SignUpForm, SuperAuthenticationForm
from .models import User


class SuperLoginView(LoginView):
    """override the default form class"""

    form_class = SuperAuthenticationForm


class SignUpView(generic.CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"


class ChangeMyMind(generic.UpdateView):
    """
    class that allowed to change the username
    """

    form_class = ChangeMyMindForm
    success_url = reverse_lazy("index")
    template_name = "profile/change_my_mind.html"

    def get_object(self, queryset=None) -> User:
        return self.request.user

    def put(self, request, *args, **kwargs):
        return super().put(request, *args, **kwargs)
