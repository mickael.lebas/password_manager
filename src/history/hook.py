from django.db import models
from django.apps import apps
import importlib
import re

from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from password_tag.models import PasswordTag
from share_password_tag.models import SharePasswordTag
from history.models import *


@receiver(pre_save, sender=SharePasswordTag)
@receiver(pre_save, sender=PasswordTag)
def on_pre_save(sender, instance, **kwargs):
    instance.old_instance = type(instance).objects.filter(pk=instance.pk).first()


pre_save.connect(on_pre_save, sender=PasswordTag)
pre_save.connect(on_pre_save, sender=SharePasswordTag)


@receiver(post_save, sender=SharePasswordTag)
@receiver(post_save, sender=PasswordTag)
def on_post_save(sender, instance, created, **kwargs):
    if not created:
        old_instance = instance.old_instance

        make_history(old_instance, instance)


post_save.connect(on_post_save, sender=PasswordTag)
post_save.connect(on_post_save, sender=SharePasswordTag)


def make_history(object_a, object_b):
    # Get type of both object
    type_a = type(object_a)
    type_b = type(object_b)

    # If there are not similar, return
    if type_a != type_b:
        return

    # Get class of object a
    model_class = object_a.__class__

    # Get name modal of object a
    class_name = model_class.__name__

    # Generate ModelHistory name
    new_class_name = class_name + "History"

    # Get special ModelName, PasswordTagHistory for exemple
    model = apps.get_model("history", class_name + "History")

    # Get definition
    new_class = type(
        new_class_name,
        (model,),
        {'__module__': model.__module__, 'Meta': type('Meta', (), {'proxy': True})}
    )

    # Create new instance
    history = new_class()

    has_change = False

    # Get properties without _state for object a
    properties_a = {
        key: value
        for (key, value) in object_a.__dict__.items()
        if not key.startswith("_")
    }

    # Get properties without _state for object b
    properties_b = {
        key: value
        for (key, value) in object_b.__dict__.items()
        if not key.startswith("_")
    }

    properties_history = {
        key: value
        for (key, value) in history.__dict__.items()
        if not key.startswith("_")
    }

    # For each properties a and b, check if it's the same or not
    for key_a in properties_a:
        value_a = properties_a[key_a]
        value_b = properties_b[key_a]
        type_a = type(value_a)
        type_b = type(value_b)

        # Cast type b with type a if isn't similar
        if type_a != type_b:
            value_b = type_a(value_b)

        # If has change, merge new data in history
        if value_a != value_b and key_a in properties_history:
            setattr(history, key_a, value_a)
            has_change = True

    # If has change, create new row
    if has_change:
        # Get formatted pk name
        pk_name = get_model_pk_name(object_a.__class__.__name__)

        # verify if exist in history model
        if pk_name in properties_history:
            setattr(history, pk_name, object_a.id)

        # save changes
        history.save()


def get_model_pk_name(camel_case):
    # Use a regular expression to search for capitals
    pattern = re.compile(r'(?<!^)(?=[A-Z])')
    # Replace capitals with underscores preceded by a lowercase letter
    snake_case = pattern.sub('_', camel_case).lower()
    # Return name foreign key id
    return snake_case + '_id'
