from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from account.models import User
from password_tag.models import PasswordTag
from share_password_tag.models import SharePasswordTag


class PasswordTagHistory(models.Model):
    username = models.TextField(_("username"))

    password = models.TextField(_("password"), max_length=128)

    url = models.TextField()

    password_tag = models.ForeignKey(
        PasswordTag,
        on_delete=models.SET_NULL,
        related_name="history",
        default=None,
        null=True
    )

    created_at = models.DateTimeField(_("created at"), default=timezone.now)


class SharePasswordTagHistory(models.Model):
    user_to = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    share_password_tag = models.ForeignKey(
        SharePasswordTag,
        on_delete=models.SET_NULL,
        related_name="history",
        default=None,
        null=True
    )

    created_at = models.DateTimeField(_("created at"), default=timezone.now)
