from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include, path, reverse_lazy

from .views import HistoryManager

urlpatterns = [
    path(
        "history/",
        HistoryManager.as_view(),
        name="history_list",
    ),
]
