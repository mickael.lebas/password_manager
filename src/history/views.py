from django.shortcuts import render
from django.apps import apps
from django.views import View
from django.db.models import QuerySet


from history.models import PasswordTagHistory, SharePasswordTagHistory
from password_tag.models import PasswordTag
from share_password_tag.models import SharePasswordTag


class HistoryManager(View):
    def get(self, request):
        history = []

        histories = []
        histories += PasswordTag.objects.filter(user=request.user)
        histories += SharePasswordTag.objects.filter(user_from=request.user)

        for data in histories:
            history += data.history.all()

        if request.user.is_superuser:
            history += PasswordTagHistory.objects.filter(password_tag=None)
            history += SharePasswordTagHistory.objects.filter(share_password_tag=None)

        if len(history) > 0:
            sorted(history, key=lambda el: el.created_at)

        return render(request, 'history/item_list.html', context={'history': history})
