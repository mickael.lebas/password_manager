import re

import django.template

register = django.template.Library()


@register.filter(name="field_type")
def field_type(field):
    """
    Template filter that returns field class name (in lower case).
    E.g. if field is CharField then {{ field|field_type }} will
    return 'charfield'.
    """
    if hasattr(field, "field") and field.field:
        return field.field.__class__.__name__.lower()
    return ""


@register.filter(name="widget_type")
def widget_type(field):
    """
    Template filter that returns field widget class name (in lower case).
    E.g. if field's widget is TextInput then {{ field|widget_type }} will
    return 'textinput'.
    """
    if (
            hasattr(field, "field")
            and hasattr(field.field, "widget")
            and field.field.widget
    ):
        return field.field.widget.__class__.__name__.lower()
    return ""


@register.filter(name="format_date")
def format_date(value, format_string):
    if value is None:
        return ""
    return value.strftime(format_string)


@register.filter(name="remove_special_characters")
def remove_special_characters(value):
    return re.sub(r"[^a-zA-Z0-9]", " ", value)


@register.filter(name="capitalize_first_letter")
def capitalize_first_letter(value):
    return value.capitalize()


@register.filter(name="type")
def class_name(value):
    return type(value).__name__
