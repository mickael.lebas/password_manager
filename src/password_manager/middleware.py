import logging

from colorama import Fore
from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import resolve
from django.utils.deprecation import MiddlewareMixin

from password_manager.utils.http_code import HttpCode, HttpMethod

EXEMPT_URLS = [settings.LOGIN_URL]
if hasattr(settings, "LOGIN_EXEMPT_URLS"):
    EXEMPT_URLS += [expr for expr in settings.LOGIN_EXEMPT_URLS]


class LoginRequiredMiddleware(MiddlewareMixin):
    """
    Middleware that requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of reverse url in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).

    Requires authentication middleware and template context processors to be
    loaded. You'll get an error if they aren't.
    """

    def process_request(self, request):
        assert hasattr(
            request, "user"
        ), "The Login Required middleware\
        requires authentication middleware to be installed. Edit your\
        MIDDLEWARE_CLASSES setting to insert\
        'django.contrib.auth.middlware.AuthenticationMiddleware'. \
        If that doesn't work, ensure your \
        TEMPLATE_CONTEXT_PROCESSORS setting includes\
        'django.core.context_processors.auth'."

        if not request.user.is_authenticated:
            if not any(
                request.path_info == exempt_url for exempt_url in EXEMPT_URLS
            ):
                return HttpResponseRedirect(settings.LOGIN_URL)


class LoggingMiddleware:
    def __init__(self, response):
        self.response = response

    def __call__(self, request):
        response = self.response(request)

        logger = logging.getLogger("django")

        status_code = getattr(response, "status_code", None)
        status_message = HttpCode().get_message(status_code)
        status_color = HttpCode().get_color(status_code)

        current_url = resolve(request.path_info).url_name

        user = request.user if request.user.is_authenticated else "Anonymous"

        method = request.method
        method_color = HttpMethod().get_color(method)

        logger.info(
            f"\n{status_color} [{status_code} | {status_message}] "
            + f"{method_color}[{method}] "
            + f"{Fore.WHITE}- {user} reatched {current_url}\n"
        )

        return response
