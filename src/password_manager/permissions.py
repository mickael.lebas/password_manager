from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied


class UserPasswordTagLinkedPermission(PermissionRequiredMixin):
    """
    class that check if user
    is linked to this proper password tag
    """

    def dispatch(self, request, *args, **kwargs):
        password_tag = self.get_object()

        if not request.user == password_tag.user:
            raise PermissionDenied

        return super().dispatch(request, *args, **kwargs)


class UserSharedTagLinkedPermission(PermissionRequiredMixin):
    """
    class that check if user
    is linked to the shared password
    """

    def dispatch(self, request, *args, **kwargs):
        share_password_tag = self.get_object()

        if not request.user == share_password_tag.user_from:
            raise PermissionDenied

        return super().dispatch(request, *args, **kwargs)


class UserSharedTagLinkedDeletePermission(PermissionRequiredMixin):
    """
    class that check if user can
    detele a shared password
    user_to and user_from can delete both
    """

    def dispatch(self, request, *args, **kwargs):
        share_password_tag = self.get_object()

        if (
            request.user != share_password_tag.user_from
            and request.user != share_password_tag.user_to
        ):
            raise PermissionDenied

        return super().dispatch(request, *args, **kwargs)
