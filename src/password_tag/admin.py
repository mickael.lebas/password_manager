from django.contrib import admin
from .models import PasswordTag


class PasswordTagAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "username",
        "password",  # TODO remove
        "url",
        "created_at",
        "user",
    )


admin.site.register(PasswordTag, PasswordTagAdmin)
