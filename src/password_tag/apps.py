from django.apps import AppConfig


class PasswordTagConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "password_tag"
