from django import forms
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import PasswordTag


class PasswordFieldForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={"autofocus": True})
    )
    url = forms.URLField(widget=forms.TextInput(attrs={"autofocus": True}))

    error_messages = {
        "password_mismatch": _("The two password fields didn’t match."),
        "same_password": _("The new password corresponds to the old one"),
    }

    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    class Meta:
        model = PasswordTag
        fields = ("username", "password1", "password2", "url")


class PasswordTagCreateForm(PasswordFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages["password_mismatch"],
                code="password_mismatch",
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get("password2")
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except ValidationError as error:
                self.add_error("password2", error)

    def save(self, commit=True):
        password_tag = super().save(commit=False)
        password_tag.set_password(self.cleaned_data["password1"])
        password_tag.set_username(password_tag.username)
        password_tag.set_url(password_tag.url)

        if commit:
            password_tag.save()
        return password_tag


class PasswordTagUpdateForm(PasswordFieldForm):
    def __init__(self, *args, **kwargs):
        # Retrieve the instance from the keyword arguments
        instance = kwargs.pop("instance", None)
        # Call the parent class's __init__ method
        super().__init__(*args, **kwargs)
        # Store the instance as an instance variable in the form
        self.instance = instance
        # Init username and url fields depending of the default instance
        self.fields["username"].initial = self.instance.username

        self.fields["password1"].initial = ""
        self.fields["password1"].required = False

        self.fields["password2"].initial = ""
        self.fields["password2"].required = False

        self.fields["username"].initial = self.instance.username
        self.fields["url"].initial = self.instance.url

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        # password are not required, so we need to check is both have values
        if password1 and not password2 or password2 and not password1:
            raise ValidationError(
                self.error_messages["password_mismatch"],
                code="password_mismatch",
            )
        # check the corresponds depending on the old and new password
        if self.instance.decrypt(self.instance.password) == password1:
            raise ValidationError(
                self.error_messages["same_password"],
                code="same_password",
            )
        return password2

    def save(self, commit=True):
        old_password_tag = PasswordTag.objects.get(id=self.instance.id)

        password_tag = super().save(commit=False)
        if self.cleaned_data["password1"]:
            password_tag.set_password(self.cleaned_data["password1"])

        if password_tag.username != old_password_tag.decrypt(
            old_password_tag.username
        ):
            password_tag.set_username(password_tag.username)
        else:
            password_tag.username = old_password_tag.username

        if password_tag.url != old_password_tag.decrypt(old_password_tag.url):
            password_tag.set_url(password_tag.url)
        else:
            password_tag.url = old_password_tag.url

        if commit:
            password_tag.save()
        return password_tag
