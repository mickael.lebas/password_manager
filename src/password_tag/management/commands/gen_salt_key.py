from django.core.management.base import BaseCommand
from cryptography.fernet import Fernet


class Command(BaseCommand):
    args = ""
    help = """
    Create a salt key
    """

    def handle(self, *args, **options):

        try:
            sel = input("Do you want to generate a salf key ? (y = yes, n = no) ")
            if sel == 'y' or sel == 'yes':
                key = Fernet.generate_key()
                print("YOUR KEY: ")
                print(key)
            else:
                print("Aborted...")
        except Exception as e:
            print(e)
            return
