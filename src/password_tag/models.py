from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from account.models import User


class PasswordTag(models.Model):
    username = models.TextField(
        _("username"),
        help_text=_(
            "Required. 150 characters or fewer. \
                Letters, digits and @/./+/-/_ only."
        ),
    )
    password = models.TextField(_("password"), max_length=128)
    # should be an url but he's encrypt
    url = models.TextField(help_text=_("Provide the url of the website"))

    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="password_tags"
    )

    def set_username(self, raw_username):
        self.username = self.encrypt(raw_username)

    def set_password(self, raw_password):
        self.password = self.encrypt(raw_password)

    def set_url(self, raw_url):
        self.url = self.encrypt(raw_url)

    def encrypt(self, string_to_encrypt):
        """encrypt the given string"""
        from cryptography.fernet import Fernet
        from django.conf import settings

        try:
            salt_key = settings.ENCRYPTED_KEY
            fernet = Fernet(salt_key)
            byte_string = string_to_encrypt.encode()
            return fernet.encrypt(byte_string)
        except Exception:
            return string_to_encrypt

    def decrypt(self, string_to_decrypt):
        """decrypt the given string"""
        from cryptography.fernet import Fernet
        from django.conf import settings

        try:
            salt_key = settings.ENCRYPTED_KEY
            fernet = Fernet(salt_key)
            # Private key is a TextField (string),
            # he's not a binary, so we need to remove characters to decrypt it
            # We removed the 2 firsts characters : "b'" and the last one : "'"
            if isinstance(string_to_decrypt, bytes):
                byte_key = string_to_decrypt
            else:
                byte_key = string_to_decrypt[2:-1].encode("utf-8")

            return fernet.decrypt(byte_key).decode()
        except Exception:
            return string_to_decrypt
