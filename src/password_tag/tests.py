from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from django.urls import reverse_lazy
from bs4 import BeautifulSoup

from account.models import User
from share_password_tag.models import SharePasswordTag

from .models import PasswordTag

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.by import By
from seleniumwire import webdriver, request


class PasswordTagModelTest(TestCase):
    """
    This class contains unit tests for the PasswordTag model.
    """

    def setUp(self):
        """
        Set up the necessary data for the tests.
        """

        self.user = User.objects.create(username="testuser")
        self.raw_username = "test_username"
        self.raw_password = "test_password"
        self.raw_url = "test_url"

        self.encrypted_string = "b'gAAAAABkgueeoN2VJIryhe4ro2DJ1czPnIB48\
            m6W9cfQiLqi9GFsSzUU6dixNBGiLil5WPEkbirTtA39dN-yVM-\
                k2YZVmLq56oyjSEplEAquDxglnLHb0UY='"

        self.encrypted_username = PasswordTag().encrypt(self.raw_username)
        self.encrypted_password = PasswordTag().encrypt(self.raw_password)
        self.encrypted_url = PasswordTag().encrypt(self.raw_url)

        self.password_tag = PasswordTag.objects.create(
            username=self.encrypted_username,
            password=self.encrypted_password,
            url=self.encrypted_url,
            user=self.user,
        )

    def test_set_username(self):
        """
        Test if setting a new username encrypts the value correctly.
        """
        new_raw_username = "new_username"
        self.password_tag.set_username(new_raw_username)
        encrypted_username = self.password_tag.encrypt(new_raw_username)
        self.assertEqual(
            self.password_tag.decrypt(self.password_tag.username),
            self.password_tag.decrypt(encrypted_username),
        )

    def test_set_password(self):
        """
        Test if setting a new password encrypts the value correctly.
        """
        new_raw_password = "new_password"
        self.password_tag.set_password(new_raw_password)
        encrypted_password = self.password_tag.encrypt(new_raw_password)
        self.assertEqual(
            self.password_tag.decrypt(self.password_tag.password),
            self.password_tag.decrypt(encrypted_password),
        )

    def test_set_url(self):
        """
        Test if setting a new URL encrypts the value correctly.
        """
        new_raw_url = "new_url"
        self.password_tag.set_url(new_raw_url)
        encrypted_url = self.password_tag.encrypt(new_raw_url)
        self.assertEqual(
            self.password_tag.decrypt(self.password_tag.url),
            self.password_tag.decrypt(encrypted_url),
        )

    def test_encrypt_decrypt(self):
        """
        Test if encrypting and decrypting data yields the original value.
        """
        decrypted_username = self.password_tag.decrypt(
            self.encrypted_username
        )
        decrypted_password = self.password_tag.decrypt(
            self.encrypted_password
        )
        decrypted_url = self.password_tag.decrypt(self.encrypted_url)

        self.assertEqual(decrypted_username, self.raw_username)
        self.assertEqual(decrypted_password, self.raw_password)
        self.assertEqual(decrypted_url, self.raw_url)

    def test_decrypt_on_string(self):
        """
        Test if decrypting a specific encrypted string returns the correct
        value.
        """
        self.assertEqual(
            self.password_tag.decrypt(self.encrypted_string),
            "Decryptemoisitulepeux!",
        )

    def test_created_at(self):
        """
        Test if the created_at field is not None and is
        before the current time.
        """
        self.assertIsNotNone(self.password_tag.created_at)

    def test_user_relation(self):
        """
        Test if the user relationship is correctly established.
        """
        self.assertEqual(self.password_tag.user, self.user)
        self.assertIn(self.password_tag, self.user.password_tags.all())


class PasswordTagManagerTest(TestCase):
    """
    This class contains unit tests for the PasswordTag manager.

    note : we can't use assertTemplateUsed because of django_jinja
    assertTemplateUsed is using django templating

    ticket: https://code.djangoproject.com/ticket/24622
    """

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.password_tag = PasswordTag.objects.create(
            username="test_username",
            password="test_password",
            url="test_url",
            user=self.user,
        )

    def test_list_view(self):
        """
        Test the list view that displays password tags for the current user.
        """
        self.client.login(username="testuser", password="testpassword")
        # follow=True for redirect 302
        response = self.client.get(reverse("item_list"), follow=True)

        self.assertEqual(response.status_code, 200)
        #
        self.assertContains(response, "test_username")
        self.assertNotContains(
            response, "test_password"
        )  # Password should be hidden
        self.assertContains(response, "test_url")

    def test_create_view(self):
        """
        Test the create view that allows users to create a new password tag.
        """
        self.client.login(username="testuser", password="testpassword")
        data = {
            "username": "new_username",
            "password": "new_password",
            "url": "new_url",
        }
        response = self.client.post(
            reverse("create_item"), data=data, follow=True
        )
        self.assertEqual(response.status_code, 200)

    def test_create_view_with_unknow_id(self):
        """
        Test the create view with unkonw id and
        check if he's registered
        """
        self.client.login(username="testuser", password="testpassword")

        # Retrieve the count of existing password tags
        existing_tags_count = PasswordTag.objects.count()

        # Attempt to create a new password tag with an invalid user ID
        invalid_user_id = 999999  # Non-existent user ID
        form_data = {
            "username": "test_username",
            "password": "test_password",
            "url": "test_url",
            "user": invalid_user_id,
        }
        response = self.client.post(reverse("create_item"), data=form_data)

        # Verify that the password tag was not
        # created due to the foreign key constraint
        self.assertEqual(response.status_code, 200)
        self.assertEqual(PasswordTag.objects.count(), existing_tags_count)

    def test_update_view(self):
        """
        Test the update view that allows users
        to update an existing password tag.
        """
        self.client.login(username="testuser", password="testpassword")
        updated_data = {
            "username": "updated_username",
            "password": "updated_password",
            "url": "updated_url",
        }
        response = self.client.post(
            reverse("update_item", args=[self.password_tag.pk]),
            data=updated_data,
        )

        self.assertEqual(response.status_code, 200)

    def test_update_view_with_unknow_id(self):
        """
        Test the update view that allows users to update
        an existing password tag.
        """
        self.client.login(username="testuser", password="testpassword")

        # Create a new password tag for the user
        password_tag = PasswordTag.objects.create(
            username="test_username",
            password="test_password",
            url="test_url",
            user=self.user,
        )

        # Retrieve the count of existing password tags
        existing_tags_count = PasswordTag.objects.count()

        # Attempt to update the password tag with an invalid user ID
        invalid_user_id = 999999  # Non-existent user ID
        form_data = {
            "username": "updated_username",
            "password": "updated_password",
            "url": "updated_url",
            "user": invalid_user_id,
        }
        response = self.client.post(
            reverse("update_item", args=[password_tag.pk]), data=form_data
        )

        # Verify that the password tag was not updated due to the
        # foreign key constraint
        self.assertEqual(response.status_code, 200)
        self.assertEqual(PasswordTag.objects.count(), existing_tags_count)

    def test_delete_view(self):
        """
        Test the delete view that allows users
        to delete an existing password tag.
        """
        self.client.login(username="testuser", password="testpassword")
        response = self.client.post(
            reverse("delete_item", args=[self.password_tag.pk])
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("item_list"))

        # Assert that the password tag has been deleted from the database
        with self.assertRaises(PasswordTag.DoesNotExist):
            PasswordTag.objects.get(pk=self.password_tag.pk)

    def test_get_password(self):
        """
        Test the get_password view that retrieves
        the decrypted password for a password tag.
        """
        self.client.login(username="testuser", password="testpassword")
        response = self.client.get(
            reverse("get_hidden_password", args=[self.password_tag.pk])
        )

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {"data": "test_password"})

    def test_get_password_share(self):
        """
        Test the get_password view for a shared password tag.
        """
        # Create a share_password_tag
        share_password_tag = SharePasswordTag.objects.create(
            user_from=self.user,
            user_to=self.user,
            password_tag=self.password_tag,
        )

        self.client.login(username="testuser", password="testpassword")
        response = self.client.get(
            reverse(
                "get_hidden_password",
                args=[share_password_tag.password_tag.pk],
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {"data": "test_password"})

    def test_get_password_not_found(self):
        """
        Test the get_password view when the password tag is not found.
        """
        self.client.login(username="testuser", password="testpassword")
        response = self.client.get(reverse("get_hidden_password", args=[999]))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.content, b"Password not found")


class SeleniumTests(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome()
        cls.selenium.implicitly_wait(10)

        cls.username = "Alexis"
        cls.password = "AlexisPY25230$"
        cls.url = "https://www.google.com"

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_login(self):
        # Register a user
        def register():
            self.selenium.get(f"{self.live_server_url}{reverse_lazy('register')}")

            username_input = self.selenium.find_element(By.NAME, "username")
            username_input.send_keys(self.username)

            password_input1 = self.selenium.find_element(By.NAME, "password1")
            password_input1.send_keys(self.password)

            password_input2 = self.selenium.find_element(By.NAME, "password2")
            password_input2.send_keys(self.password)

            submit_button = self.selenium.find_element(By.CSS_SELECTOR, '.button.is-success')
            submit_button.click()

        # Login
        def login():
            username_input = self.selenium.find_element(By.NAME, "username")
            username_input.send_keys(self.username)

            password_input = self.selenium.find_element(By.NAME, "password")
            password_input.send_keys(self.password)

            submit_button = self.selenium.find_element(By.CSS_SELECTOR, '.button.is-success')
            submit_button.click()

        # Test authentication
        def test_is_authenticated():
            # Get response into last redirect with url "item_list"
            for request in self.selenium.requests:
                if request.response and request.url == f"{self.live_server_url}{reverse_lazy('item_list')}":
                    body = request.response.body

                    soup = BeautifulSoup(body, 'html.parser')

                    is_authenticated = soup.find('body').attrs['data-user']

                    # Check user is authenticated
                    self.assertTrue(is_authenticated, 'True')

        # Create password tag
        def create_item():
            self.selenium.get(f"{self.live_server_url}{reverse_lazy('create_item')}")

            username_input = self.selenium.find_element(By.NAME, "username")
            username_input.send_keys(self.username)

            password_input1 = self.selenium.find_element(By.NAME, "password1")
            password_input1.send_keys(self.password)

            password_input2 = self.selenium.find_element(By.NAME, "password2")
            password_input2.send_keys(self.password)

            url_input = self.selenium.find_element(By.NAME, "url")
            url_input.send_keys(self.url)

            submit_button = self.selenium.find_element(By.CSS_SELECTOR, '.button.is-success')
            submit_button.click()

        def item_list():
            password_tags = PasswordTag.objects.all()
            self.assertTrue(1, len(password_tags))

            password_tag = password_tags[0]
            self.assertEqual(self.username, password_tag.decrypt(password_tag.username))

        register()
        login()
        test_is_authenticated()
        create_item()
        item_list()
