from django.urls import path

from .views import PasswordTagManager

urlpatterns = [
    path(
        "item_list/",
        PasswordTagManager.ListView.as_view(),
        name="item_list",
    ),
    path(
        "create_item/",
        PasswordTagManager.CreateView.as_view(),
        name="create_item",
    ),
    path(
        "update_item/<int:pk>",
        PasswordTagManager.UpdateView.as_view(),
        name="update_item",
    ),
    path(
        "hidden_password/<int:id>",
        PasswordTagManager.get_password,
        name="get_hidden_password",
    ),
    path(
        "delete_item/<int:pk>",
        PasswordTagManager.DeleteView.as_view(),
        name="delete_item",
    ),
]
