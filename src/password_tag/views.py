from django.http import HttpResponseNotFound, JsonResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import generic


from pprint import pprint


from password_manager.permissions import UserPasswordTagLinkedPermission

from .form import PasswordTagCreateForm, PasswordTagUpdateForm
from .models import PasswordTag


class PasswordTagManager:
    class ListView(generic.ListView):
        """List View"""

        template_name = "password_tag/item_list.html"
        context_object_name = "password_tags"

        def get_queryset(self):
            # get password_tags from user
            password_tags = self.request.user.password_tags.all()

            for password_tag in password_tags:
                password_tag.username = password_tag.decrypt(
                    password_tag.username
                )
                # set password to nothing, just for the display
                password_tag.password = "*****************************"
                password_tag.url = password_tag.decrypt(password_tag.url)

            return password_tags

    class CreateView(generic.CreateView):
        """create view"""

        model = PasswordTag
        form_class = PasswordTagCreateForm
        template_name = "password_tag/create_item.html"
        success_url = reverse_lazy("item_list")

        def form_valid(self, form):
            form.instance.user = self.request.user
            return super().form_valid(form)

    class UpdateView(UserPasswordTagLinkedPermission, generic.UpdateView):
        """update view"""

        model = PasswordTag
        form_class = PasswordTagUpdateForm
        template_name = "password_tag/update_item.html"
        success_url = reverse_lazy("item_list")
        permission_required = ()

        def get_object(self, queryset=None):
            """Return the current model"""
            obj = self.model.objects.get(id=self.kwargs["pk"])
            obj.username = obj.decrypt(obj.username)
            obj.url = obj.decrypt(obj.url)
            return obj

    class DeleteView(UserPasswordTagLinkedPermission, generic.DeleteView):
        """delete view"""

        model = PasswordTag
        success_url = reverse_lazy("item_list")
        permission_required = ()
        """
        override the default template which is passwordtag_confirm_delete.html
        by "password_tag/item_list.html" to pass test
        because the default is called internally
        before running success_url
        it allows to not create an empty template just for test
        """
        template_name = "password_tag/item_list.html"

        def form_valid(self, form):
            success_url = self.get_success_url()

            # To keep safe history, remove foreign key for all objects
            for history in self.object.history.all():
                history.password_tag = None
                history.save()

            # To keep safe history, remove foreign key for all objects
            for share_password_tag in self.object.share_password_tag.all():
                for history in share_password_tag.history.all():
                    history.share_password_tag = None
                    history.save()

                # Safe delete
                share_password_tag.delete()

            self.object.delete()
            return HttpResponseRedirect(success_url)

    def get_password(request, id):
        """
        Decrypt the password and send it
        """
        try:
            # get the password_tag
            password_tag = request.user.password_tags.filter(id=id).first()
            # is none check in share_password
            if password_tag is None:
                share_password_tag = request.user.shares_received.filter(
                    password_tag_id=id
                ).first()
                # if share_password_tag return the decrypt
                if share_password_tag is not None:
                    share_password_tag.password_tag.password = (
                        share_password_tag.password_tag.decrypt(
                            share_password_tag.password_tag.password
                        )
                    )
                    return JsonResponse(
                        {"data": share_password_tag.password_tag.password}
                    )
                return HttpResponseNotFound("Password not found")
            else:
                # return password_tag
                password_tag.password = password_tag.decrypt(
                    password_tag.password
                )
                return JsonResponse({"data": password_tag.password})

        except PasswordTag.DoesNotExist:
            return HttpResponseNotFound("Password not found")
