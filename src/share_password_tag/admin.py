from django.contrib import admin

from .models import SharePasswordTag


class SharePasswordTagAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "user_from",
        "user_to",
    )


admin.site.register(SharePasswordTag, SharePasswordTagAdmin)
