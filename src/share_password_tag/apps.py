from django.apps import AppConfig


class SharePasswordTagConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "share_password_tag"
