from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from account.models import User
from share_password_tag.models import SharePasswordTag


class SharePasswordTagCreateForm(forms.ModelForm):
    error_messages = {
        "user_not_found": _("The username doesn'not exist"),
    }
    user_to = forms.CharField(
        widget=forms.Select(
            attrs={"placeholder": "Start typing..."},
            choices=[],
        ),
    )
    # Hidden field, you can edit it by the front console but the back will
    # reject your request
    password_tag_id = forms.IntegerField(
        widget=forms.HiddenInput(attrs={"readonly": "readonly"}), label=""
    )

    class Meta:
        model = SharePasswordTag
        fields = ("user_to", "password_tag_id")

    def __init__(self, *args, **kwargs):
        # get pk and init password_tag_id to the pk value
        pk = kwargs.pop("pk", None)
        super().__init__(*args, **kwargs)
        self.fields["password_tag_id"].initial = pk
        self.fields["user_to"].widget.choices = get_user_choices()

    def clean_user_to(self):
        """
        user_to is a string (id)
        We need to return an instance
        """
        user_id = self.cleaned_data.get("user_to")

        try:
            user_instance = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise ValidationError(
                self.error_messages["user_not_found"],
                code="user_not_found",
            )

        return user_instance

    def save(self, commit=True):
        share_password_tag = super().save(commit=False)
        if commit:
            share_password_tag.save()
        return share_password_tag


class SharePasswordTagUpdateForm(forms.ModelForm):
    error_messages = {
        "user_not_found": _("The username doesn'not exist"),
    }
    user_to_username = forms.CharField(
        widget=forms.Select(
            attrs={"placeholder": "Start typing..."},
            choices=[],
        ),
        label="Share_with",
    )

    class Meta:
        model = SharePasswordTag
        fields = ("user_to_username",)

    def __init__(self, *args, **kwargs):
        # Retrieve the instance from the keyword arguments
        kwargs.pop("pk", None)
        # instance = kwargs.pop("instance", None)
        super().__init__(*args, **kwargs)
        # init choices
        self.fields["user_to_username"].widget.choices = get_user_choices()
        # init select value
        self.fields["user_to_username"].initial = kwargs[
            "instance"
        ].user_to_id

    def clean_user_to_username(self):
        """
        user_to is a string (username)
        We need to return an instance
        """
        user_id = self.cleaned_data.get("user_to_username")

        try:
            user_instance = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise ValidationError(
                self.error_messages["user_not_found"],
                code="user_not_found",
            )
        return user_instance

    def save(self, commit=True):
        share_password_tag = super().save(commit=False)
        share_password_tag.user_to = self.cleaned_data["user_to_username"]
        if commit:
            share_password_tag.save()
        return share_password_tag


def get_user_choices():
    """return choices for the select field"""
    queryset = User.objects.filter(username__istartswith="")

    choices = [(user.id, user.username) for user in queryset]

    return choices
