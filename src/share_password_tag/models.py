from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from account.models import User
from password_tag.models import PasswordTag


class SharePasswordTag(models.Model):
    user_from = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="shares_sent"
    )
    user_to = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="shares_received"
    )

    password_tag = models.ForeignKey(
        PasswordTag, on_delete=models.CASCADE, related_name="share_password_tag"
    )
    created_at = models.DateTimeField(_("created at"), default=timezone.now)
