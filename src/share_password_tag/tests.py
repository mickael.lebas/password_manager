from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone

from account.models import User
from password_tag.models import PasswordTag

from .models import SharePasswordTag


class SharePasswordTagModelTest(TestCase):
    """
    This class contains unit tests for the SharePasswordTag model.
    """

    def setUp(self):
        """
        Set up the necessary data for the tests.
        """
        self.user_from = User.objects.create(username="John")
        self.user_to = User.objects.create(username="Jane")
        self.password_tag = PasswordTag.objects.create(
            username="test_username",
            password="test_password",
            url="test_url",
            user=self.user_from,
        )

        self.share_password_tag = SharePasswordTag.objects.create(
            user_from=self.user_from,
            user_to=self.user_to,
            password_tag=self.password_tag,
        )

    def test_user_from(self):
        """
        Test if the user_from field is set correctly.
        """
        self.assertEqual(self.share_password_tag.user_from, self.user_from)

    def test_user_to(self):
        """
        Test if the user_to field is set correctly.
        """
        self.assertEqual(self.share_password_tag.user_to, self.user_to)

    def test_password_tag(self):
        """
        Test if the password_tag field is set correctly.
        """
        self.assertEqual(
            self.share_password_tag.password_tag, self.password_tag
        )

    def test_created_at(self):
        """
        Test if the created_at field is
        not None and is before the current time.
        """
        self.assertIsNotNone(self.share_password_tag.created_at)


class SharePasswordTagManagerTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create_user(
            username="user1", password="testpassword"
        )
        self.user2 = User.objects.create_user(
            username="user2", password="testpassword"
        )
        self.password_tag = PasswordTag.objects.create(
            username="test_username",
            password="test_password",
            url="test_url",
            user=self.user1,
        )
        self.share_password_tag = SharePasswordTag.objects.create(
            user_from=self.user1,
            user_to=self.user2,
            password_tag=self.password_tag,
        )

    def test_list_view(self):
        """
        Test the list view that displays shared password tags.
        """
        self.client.login(username="user1", password="testpassword")
        response = self.client.get(reverse("shared_items"))

        self.assertEqual(response.status_code, 200)

    def test_create_view(self):
        """
        Test the create view that allows users to share a password tag.
        """
        self.client.login(username="user1", password="testpassword")
        response = self.client.post(
            reverse("create_share", kwargs={"pk": self.password_tag.id}),
            {
                "user_to": self.user2.id,
                "password_tag_id": self.password_tag.id,
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse("shared_items"))

    def test_update_view(self):
        """
        Test the update view that allows users to modify a shared password tag.
        """
        self.client.login(username="user1", password="testpassword")
        response = self.client.post(
            reverse(
                "update_share", kwargs={"pk": self.share_password_tag.id}
            ),
            {
                "user_to": self.user1.id,
            },
        )

        self.assertEqual(response.status_code, 200)

    def test_delete_view(self):
        """
        Test the delete view that allows users to remove a shared password tag.
        """
        self.client.login(username="user1", password="testpassword")
        response = self.client.post(
            reverse("delete_share", kwargs={"pk": self.share_password_tag.id})
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse("shared_items"))
