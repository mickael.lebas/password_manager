from django.urls import path

from .views import SharePasswordTagManager

urlpatterns = [
    path(
        "shared_items/",
        SharePasswordTagManager.ListView.as_view(),
        name="shared_items",
    ),
    path(
        "create_item/<int:pk>",
        SharePasswordTagManager.CreateView.as_view(),
        name="create_share",
    ),
    path(
        "update_item/<int:pk>",
        SharePasswordTagManager.UpdateView.as_view(),
        name="update_share",
    ),
    path(
        "delete_item/<int:pk>",
        SharePasswordTagManager.DeleteView.as_view(),
        name="delete_share",
    ),
]
