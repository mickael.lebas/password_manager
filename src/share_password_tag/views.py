from django.db.models import Q
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views import generic

from password_manager.permissions import (
    UserSharedTagLinkedDeletePermission,
    UserSharedTagLinkedPermission,
)
from password_tag.models import PasswordTag

from .form import SharePasswordTagCreateForm, SharePasswordTagUpdateForm
from .models import SharePasswordTag


class SharePasswordTagManager:
    class ListView(generic.ListView):
        """List View"""

        template_name = "share_password_tag/shared_items.html"
        context_object_name = "shared_password_tags"

        def get_queryset(self):
            # get password_tags from user
            shared_password_tags = SharePasswordTag.objects.filter(
                Q(user_from=self.request.user) | Q(user_to=self.request.user)
            )

            for item in shared_password_tags:
                # Decrypt and assign the modified username and URL
                item.password_tag.username = item.password_tag.decrypt(
                    item.password_tag.username
                )
                item.password_tag.url = item.password_tag.decrypt(
                    item.password_tag.url
                )
                # set password to nothing, just for the display
                item.password_tag.password = "*****************************"

                """
                include usernames from user_to and user_from as additional
                attributes
                """
                item.user_to_username = item.user_to.username
                item.user_from_username = item.user_from.username

            return shared_password_tags

    class CreateView(generic.CreateView):
        """create view"""

        model = SharePasswordTag
        form_class = SharePasswordTagCreateForm
        template_name = "share_password_tag/create_share.html"
        success_url = reverse_lazy("shared_items")

        def get_form_kwargs(self):
            # send pk to the form (pk = id)
            kwargs = super().get_form_kwargs()
            kwargs["pk"] = self.kwargs["pk"]
            return kwargs

        def form_valid(self, form):
            form.instance.user_from = self.request.user
            form.instance.user_to = form.cleaned_data["user_to"]
            password_tag = get_object_or_404(
                PasswordTag, id=form.cleaned_data["password_tag_id"]
            )
            # failSafe to check if the provided id belongs to the current user
            if not password_tag.user == self.request.user:
                return HttpResponseNotFound("GET BACK !")
            form.instance.password_tag = password_tag
            return super().form_valid(form)

    class UpdateView(UserSharedTagLinkedPermission, generic.UpdateView):
        """update view"""

        model = SharePasswordTag
        form_class = SharePasswordTagUpdateForm
        template_name = "share_password_tag/update_item.html"
        success_url = reverse_lazy("shared_items")
        permission_required = ()

        def get_form_kwargs(self):
            # send pk to the form (pk = id)
            kwargs = super().get_form_kwargs()
            kwargs["pk"] = self.kwargs["pk"]
            return kwargs

        def get_object(self, queryset=None):
            """Return the current model"""
            obj = self.model.objects.get(id=self.kwargs["pk"])
            obj.user_to_username = obj.user_to.username
            return obj

    class DeleteView(UserSharedTagLinkedDeletePermission, generic.DeleteView):
        """
        delete view
        this class not inherit from UserSharedTagLinkedPermission
        because a user_to and user_from can delete both
        """

        model = SharePasswordTag
        success_url = reverse_lazy("shared_items")
        permission_required = ()

        def form_valid(self, form):
            success_url = self.get_success_url()

            # Safe delete
            self.object.password_tag = None

            # To keep safe history, remove foreign key for all objects
            for history in self.object.history.all():
                history.share_password_tag = None
                history.save()

            self.object.delete()
            return HttpResponseRedirect(success_url)
