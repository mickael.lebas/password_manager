document.addEventListener('DOMContentLoaded', () => {
    /*================== Password strength ==================*/
    // get all paswword inputs
    const passwordInputs = document.querySelectorAll('input[type="password"]');

    if (passwordInputs) {
        // add keyup listener
        passwordInputs.forEach(inputElement => {
            inputElement.addEventListener('keyup', event => {
                // get the strength of the given value (password)
                let passwordStrength = checkPasswordStrength(event.target.value)
                // get the progress bar of the password field
                const progressBar = document.getElementById(`progress-bar-${inputElement.id}`);
                // set a percentage (5 is the highest score)
                const percentage = (passwordStrength / 5) * 100
                // set the percentage to the progress bar
                progressBar.value = percentage

                /**
                 * reset and set the color depending of the percentage
                 * bad : 0% -> 30%
                 * medium : 30% -> 70%
                 * high : 70% -> 100%
                 */
                progressBar.classList.remove('is-danger', 'is-warning', 'is-success');

                if (percentage >= 0 && percentage < 30) {
                    progressBar.classList.add('is-danger');
                } else if (percentage >= 30 && percentage < 70) {
                    progressBar.classList.add('is-warning');
                } else {
                    progressBar.classList.add('is-success');
                }
            });
        });
    }
    /*================= END Password strength ================*/

    let items = document.querySelectorAll('.button.is-info')
    items.forEach((item) => {
        item.addEventListener('click', (e) => {
            let id = e.target.dataset['id']
            let input = document.getElementById(id)
            if (input) {
                if (input.type === 'password') input.type = 'text'
                else input.type = 'password'
            }
        })
    })
})

/**
 * Return the score of the given string
 * @param {*} string
 * @returns
 */
function checkPasswordStrength(string) {

    let password_length = 0;

    const lowerRegex = '[a-z]';
    const upperRegex = '[A-Z]';
    const numberRegex = '[0-9]';
    const specialRegex = /[#!?@$%^&*\-_]/;

    if (string.match(lowerRegex) !== null) password_length += 1;
    if (string.match(upperRegex) !== null) password_length += 1;
    if (string.match(numberRegex) !== null) password_length += 1;
    if (string.match(specialRegex) !== null) password_length += 1;
    if (string.length >= 12) password_length += 1;

    return password_length;
}