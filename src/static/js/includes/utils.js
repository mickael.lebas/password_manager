/*=========================================== FUNCTIONS ===========================================*/

/**
 * get the password
 * @param {*} id 
 * @returns 
 */
window.getPassword = function(id) {

    return fetch(`/hidden_password/${id}`, {
        method: "GET",
        headers: {
            'X-CSRFToken': getCSRF(),
            "Accept": "application/json",
            'Content-Type': 'application/json'
        },
    })
    .then(response => response.json())
    .then(response => {
        return response.data
    })
    .catch(error => {
        console.error("Error get hidden password : ", error);
        return ""
    })

}

/**
 * Return csrf token from cookies
 * @returns 
 */
window.getCSRF = function() {
    
    let csrftoken = null;
    let cookieValue = null;
    let cookieRegex = /csrftoken=([^;\s]+)/;
    
    let cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i].trim();
        let match = cookie.match(cookieRegex);
        if (match) {
            cookieValue = match[1];
            break;
        }
    }

    if (cookieValue) {
        csrftoken = decodeURIComponent(cookieValue);
    }

    return csrftoken
}

/*=========================================== FUNCTIONS ===========================================*/