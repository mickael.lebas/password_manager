document.addEventListener('DOMContentLoaded', () => {
    // Functions to open and close a modal
    function openModal($el, event) {
        $el.classList.add('is-active');

        let btn = event.target.nodeName !== 'BUTTON'
            ? event.target.parentNode
            : event.target

        $el.querySelector('#form').setAttribute('action', btn.dataset.url)
        $el.querySelector('.modal-card-title').innerText = btn.dataset.title
        $el.querySelector('.message-body').innerText = btn.dataset.body
    }

    function closeModal($el) {
        $el.classList.remove('is-active');
    }

    // Add a click event on buttons to open a specific modal
    (document.querySelectorAll('[data-target="delete-modal"]') || []).forEach(($trigger) => {
        const modal = $trigger.dataset.target;
        const $target = document.getElementById(modal);

        $trigger.addEventListener('click', (event) => {
            openModal($target, event);
        });
    });

    // Add a click event on various child elements to close the parent modal
    (document.querySelectorAll('.modal-card-head .delete, form .is-light') || []).forEach(($close) => {
        const $target = $close.closest('.modal');

        $close.addEventListener('click', () => {
            closeModal($target);
        });
    });
});