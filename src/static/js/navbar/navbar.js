document.addEventListener('DOMContentLoaded', () => {
    const navbar = document.querySelector('.navbar-burger');
    const navbarMenu = document.querySelector('.navbar-menu');

    navbar.addEventListener('click', function () {
        this.classList.toggle('is-active')
        if(this.classList.contains('is-active')) {
            navbarMenu.style.display = 'block'
        } else {
            navbarMenu.style.display = 'none'
        }
    })
})