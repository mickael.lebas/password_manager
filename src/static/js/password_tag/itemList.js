/*============================================= DOM ==============================================*/

let urlsInTable = null
let urlValues = []
let table = null
let rowsOfTable = null
let orderedRows = null

document.addEventListener('DOMContentLoaded', () => {
    //!!!!!!!!!!! FOR SEARCH PART
    table = document.querySelector("table");
    // get all dom urls
    urlsInTable = document.querySelectorAll(`[data-id="data-url"]`)
    // get all urls values
    urlsInTable.forEach(url => {
        urlValues.push(url.href)
    });
    //!!!!!!!!!!! END SEARCH PART

    initPasswordToggle()

    initSearch()
})

/*============================================== DOM ==============================================*/

function initSearch() {
    let input = document.getElementById('search-input')
    input.addEventListener('keyup', search)

    let button = document.getElementById('search-button')
    button.addEventListener('click', search)
}

function initPasswordToggle() {
    // get button from the dom
    const toogleShowPassword = document.querySelectorAll("#toogleShowPassword")
    // get td element when dom is loaded to keep the default value
    const passwordCells = document.querySelectorAll('td .td-password');
    const tdDefaultValue = passwordCells[0].textContent

    toogleShowPassword.forEach(domElement => {
        let isHidden = true
        // create listener to link click and the getPassword function
        domElement.addEventListener('click', async () => {
            // mutate the value
            isHidden = !isHidden
            // get id from the tr parent
            const trElement = domElement.closest('tr')
            const id = trElement.dataset.id
            // get td element
            const tdElement = trElement.querySelector(`[data-id="password-${id}"]`)

            isHidden ?
                tdElement.textContent = tdDefaultValue
                : tdElement.textContent = await getPassword(id)
        })
    });
}

/**
 * Search function based on string similarity project
 * Display none element that do not match
 * And orders the tr elements depending on their match rating
 */
function search() {
    // reset
    displayAllTr();

    // Get the search input value
    let searchQuery = document.getElementById('search-input').value;
    if (searchQuery) {
        const bestMatches = stringSimilarity.findBestMatch(searchQuery, urlValues);

        // filter and sort the matches based on rating
        const matchedUrls = bestMatches.ratings
            .filter(match => match.rating > 0)
            .sort((a, b) => b.rating - a.rating)
            .map(match => match.target);

        rowsOfTable = Array.from(table.getElementsByTagName("tr"));

        // hide the tr if is not found inside matchedUrls
        urlsInTable.forEach(element => {
            const trElement = element.closest('tr');
            trElement.style.display = matchedUrls.includes(element.href) ? "" : "none";
        });

        // sort tr by order of this rating
        orderedRows = rowsOfTable
            .filter(element => element.querySelector('[data-id="data-url"]'))
            .sort((a, b) => {
                const aUrl = a.querySelector('[data-id="data-url"]').href;
                const bUrl = b.querySelector('[data-id="data-url"]').href;
                return matchedUrls.indexOf(aUrl) - matchedUrls.indexOf(bUrl);
            });
        // append ordered rows to table
        orderedRows.forEach(row => {
            table.appendChild(row);
        });
    }
}

/**
 * Display all hidden tr
 */
function displayAllTr() {
    urlsInTable.forEach(element => {
        const trElement = element.closest('tr')
        trElement.style.display = ""
    });
}