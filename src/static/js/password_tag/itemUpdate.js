
/*============================================= DOM ==============================================*/

let isHidden = true

document.addEventListener('DOMContentLoaded', function() {
    // get button from the dom
    const toogleShowPassword = document.getElementById("toogleShowPassword")
    // get td element when dom is loaded to keep the default value
    const defaultSpan = document.getElementById("password")
    const defaultSpanValue = document.getElementById("password").textContent
    
    // create listener to link click and the getPassword function
    toogleShowPassword.addEventListener('click', async () => {
        // mutate the value
        isHidden = !isHidden

        const id = defaultSpan.dataset.id
        // get td element
        const spanElement = document.getElementById(`password`)

        isHidden ? 
        spanElement.textContent = defaultSpanValue 
            : spanElement.textContent = await getPassword(id)        
    })
})

/*============================================== DOM ==============================================*/